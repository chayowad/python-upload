FROM python:3.9.5
  
WORKDIR /usr/src/app

RUN mkdir -p /app/uploads

COPY . .

RUN pip install -r requirements.txt

CMD [ "python", "./upload.py" ]

